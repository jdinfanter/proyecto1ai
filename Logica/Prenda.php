<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/PrendaDAO.php";
class Prenda{
    private $idPrenda;
    private $nombre;
    private $precio;
    private $descripcion;
    private $foto;
    private $tipo;
    private $genero;
    private $conexion;
    private $prendaDAO;
    
    public function getIdPrenda(){
        return $this -> idPrenda;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getPrecio(){
        return $this -> precio;
    }
    
    public function getDescripcion(){
        return $this -> descripcion;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getTipo(){
        return $this -> tipo;
    }

    public function getGenero(){
        return $this -> genero;
    }



        
    public function Prenda($idPrenda = "", $nombre = "", $precio = "", $descripcion = "", $foto ="", $tipo = "", $genero = ""){
        $this -> idPrenda = $idPrenda;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> descripcion = $descripcion;
        $this -> foto = $foto;
        $this -> tipo = $tipo;
        $this -> genero = $genero;
        $this -> conexion = new Conexion();
        $this -> prendaDAO = new PrendaDAO($this -> idPrenda, $this -> nombre, $this -> precio, $this -> descripcion, $this -> foto, $this -> tipo, $this -> genero);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> prendaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> descripcion = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> tipo = $resultado[6];  
        $this -> genero = $resultado[7]; 
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> prendaDAO -> insertar());  
        $this -> conexion -> ejecutar($this -> prendaDAO -> consultarId());       
        $this -> conexion -> cerrar();  
        $resultado = $this -> conexion -> extraer();
        return $resultado[0];  
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Prenda($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> prendaDAO -> consultarPaginacion($cantidad, $pagina));
        $prendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Prenda($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[6], $resultado[7]);
            array_push($prendas, $p);
        }
        $this -> conexion -> cerrar();
        return $prendas;
    }

    public function consultarFiltro($cantidad, $pagina, $filtro, $filtro2){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> prendaDAO -> consultarFiltro($cantidad, $pagina, $filtro, $filtro2));
        $prendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Prenda($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[6], $resultado[7]);
            array_push($prendas, $p);
        }
        $this -> conexion -> cerrar();
        return $prendas;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> prendaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> prendaDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
}

?>