<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/GeneroDAO.php";

class Genero{
    private $idGenero;
    private $genero;
    private $conexion;
    private $generoDAO;
    
    public function getIdGenero(){
        return $this -> idGenero;
    }
    
    public function getGenero(){
        return $this -> genero;
    }
   
        
    public function Genero($idGenero = "", $genero = ""){
        $this -> idGenero = $idGenero;
        $this -> genero = $genero;
        $this -> conexion = new Conexion();
        $this -> generoDAO = new GeneroDAO($this -> idGenero, $this -> genero);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> generoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idTGenero = $resultado[0];
        $this -> genero = $resultado[1];
   
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> generoDAO -> consultarTodos());
        $tipos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Genero($resultado[0], $resultado[1]);
            array_push($tipos, $t);
        }
        $this -> conexion -> cerrar();        
        return $tipos;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> generoDAO -> insertar());        
        $this -> conexion -> cerrar();    
    }
    

}

?>