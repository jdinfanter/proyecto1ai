<?php

require_once "Persistencia/Conexion.php";
require_once "Persistencia/VentaDAO.php";

class Venta
{
    private $idVenta;
    private $fecha;
    private $precioT;
    private $cliente;
    private $prendas;
    private $conexion;
    private $ventaDAO;

    public function getIdVenta()
    {
        return $this->idVenta;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getPrecioT()
    {
        return $this->precioT;
    }

    public function getCliente()
    {
        return $this->cliente;
    }

    public function getPrenda()
    {
        return $this->prendas;
    }

    public function Venta($idVenta = "", $fecha = "", $precioT = "", $cliente, $prendas)
    {
        $this->idVenta = $idVenta;
        $this->fecha = $fecha;
        $this->precioT = $precioT;
        $this->cliente = $cliente;
        $this->prendas = $prendas;
        $this->conexion = new Conexion();
        $this->ventaDAO = new VentaDAO($this->idVenta, $this->fecha, $this->precioT, $this->cliente);
    }

   

    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ventaDAO -> insertar());  
        $this -> conexion -> ejecutar($this -> ventaDAO -> consultarId());       
        $this -> conexion -> cerrar();  
        $resultado = $this -> conexion -> extraer();
        return $resultado[0];  
    }

    public function consultarPaginacion($cantidad, $pagina)
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->ventaDAO->consultarPaginacion($cantidad, $pagina));
        $ventas = array();
        while (($resultado = $this->conexion->extraer()) != null) {
            $cliente = new Cliente("", $resultado[3], $resultado[4]);
            $c = new Venta($resultado[0], $resultado[1], $resultado[2], $cliente, "");
            array_push($ventas, $c);
        }
        $this->conexion->cerrar();
        return $ventas;
    }

    public function consultarCantidad()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->ventaDAO->consultarCantidad());
        $this->conexion->cerrar();
        return $this->conexion->extraer()[0];
    }
}
