<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/VentaPrendaDAO.php";

class VentaPrenda{
    private $nombre_cliente;
    private $apellido_cliente;
    private $nombre_prenda;
    private $talla_prenda;
    private $fecha_venta;
    private $precio_total;
    private $idVenta;
    private $idPrenda;
    private $idTalla;
    private $cantidad;
    private $precio;
    
    public function getNombreCliente(){
        return $this -> nombre_cliente;
    }
    
    public function getApellidoCliente(){
        return $this -> apellido_cliente;
    }
    
    public function getNombrePrenda(){
        return $this -> nombre_prenda;
    }
    
    public function getTallaPrenda(){
        return $this -> talla_prenda;
    }
    
    public function getFechaVenta(){
        return $this ->fecha_venta;
    }
    
    public function getPrecioTotal(){
        return $this ->precio_total;
    }
    
    public function getIdVenta(){
        return $this -> idVenta;
    }
    
    public function getIdPrenda(){
        return $this -> idPrenda;
    }

    public function getIdTalla(){
        return $this -> idTalla;
    }

    public function getCantidad(){
        return $this -> cantidad;
    }

    public function getPrecio(){
        return $this -> precio;
    }

        
   /* public function VentaPrenda($idVenta = "", $idPrenda = "", $idTalla, $cantidad ="", $precio = ""){
        $this -> idVenta = $idVenta;
        $this -> idPrenda = $idPrenda;
        $this -> idTalla = $idTalla;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> conexion = new Conexion();
        $this -> ventaprendaDAO = new VentaPrendaDAO($this -> idVenta, $this -> idPrenda, $this -> idTalla, $this -> cantidad, $this -> precio);
    }*/
    
    public function VentaPrenda($nombre_cliente="",$apellido_cliente="",$nombre_prenda="",$talla_prenda="",$fecha_venta="",$precio_total="",  $idVenta = "", $idPrenda = "", $idTalla="", $cantidad ="", $precio = ""){
        $this -> nombre_cliente = $nombre_cliente;
        $this -> apellido_cliente= $apellido_cliente;
        $this -> nombre_prenda = $nombre_prenda;
        $this -> talla_prenda = $talla_prenda;
        $this -> fecha_venta = $fecha_venta;
        $this -> precio_total= $precio_total;
        $this -> idVenta = $idVenta;
        $this -> idPrenda = $idPrenda;
        $this -> idTalla = $idTalla;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> conexion = new Conexion();
        $this -> ventaprendaDAO = new VentaPrendaDAO($this -> nombre_cliente,  $this -> apellido_cliente, $this -> nombre_prenda, $this -> talla_prenda,  $this -> fecha_venta, $this -> precio_total, $this -> idVenta, $this -> idPrenda, $this -> idTalla, $this -> cantidad, $this -> precio);
    }

    public function consultar($idVenta){
     $this -> conexion -> abrir();
     $this -> conexion -> ejecutar($this -> ventaprendaDAO -> consultar($idVenta));
     $this -> conexion -> cerrar();
     $resultado = $this -> conexion -> extraer();
     $this -> nombre_cliente = $resultado[0];
     $this -> apellido_cliente= $resultado[1];
     $this -> nombre_prenda = $resultado[2];
     $this -> talla_prenda = $resultado[3];
     $this -> fecha_venta = $resultado[4];
     $this -> precio_total = $resultado[5];
     $this -> cantidad = $resultado[6];
     $this -> precio = $resultado[7];
     }

     
     public function consultarArray($idVenta){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> ventaprendaDAO -> consultar($idVenta));
         $ventaprendas = array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             $c = new VentaPrenda($resultado[0], $resultado[1], $resultado[2],$resultado[3], $resultado[4],$resultado[5],"",$resultado[8],$resultado[9],$resultado[6],$resultado[7] );
             array_push($ventaprendas, $c);
         }
         $this -> conexion -> cerrar();
         return $ventaprendas;
     }
     
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ventaprendaDAO -> consultarTodos());
        $tallas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            // $t = new Talla($resultado[0], $resultado[1]);
            // array_push($tallas, $t);
        }
        $this -> conexion -> cerrar();        
        return $tallas;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ventaprendaDAO -> insertar());       
        $this -> conexion -> cerrar();    
    }
    

}

?>