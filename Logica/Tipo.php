<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/TipoDAO.php";

class Tipo{
    private $idTipo;
    private $tipo;
    private $conexion;
    private $tipoDAO;
    
    public function getIdTipo(){
        return $this -> idTipo;
    }
    
    public function getTipo(){
        return $this -> tipo;
    }
   
        
    public function Tipo($idTipo = "", $tipo = ""){
        $this -> idTipo = $idTipo;
        $this -> tipo = $tipo;
        $this -> conexion = new Conexion();
        $this -> tipoDAO = new TipoDAO($this -> idTipo, $this -> tipo);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tipoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idTipo = $resultado[0];
        $this -> tipo = $resultado[1];
   
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tipoDAO -> consultarTodos());
        $tipos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Tipo($resultado[0], $resultado[1]);
            array_push($tipos, $t);
        }
        $this -> conexion -> cerrar();        
        return $tipos;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> tipoDAO -> insertar());        
        $this -> conexion -> cerrar();    
    }
    

}

?>