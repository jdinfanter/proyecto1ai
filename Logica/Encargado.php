<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/EncargadoDAO.php";

class Encargado{
    private $idEncargado;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;      
    private $conexion;
    private $encargadoDAO;

    public function getIdEncargado(){
        return $this -> idEncargado;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Encargado($idEncargado= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idEncargado = $idEncargado;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> encargadoDAO = new EncargadoDAO($this -> idEncargado, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    }
   
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO -> registrar());
        $this -> conexion -> cerrar();
    }
   
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idEncargado = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> nombre = $resultado[2];
            $this -> apellido = $resultado[3];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> clave = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> estado = $resultado[5];
        
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO -> consultarPaginacion($cantidad, $pagina));
        $encargados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Encargado($resultado[0], $resultado[1], $resultado[2],$resultado[3],"","", $resultado[4]);
            array_push($encargados, $e);
        }
        $this -> conexion -> cerrar();
        return $encargados;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->encargadoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO-> editar_clave());
        $this -> conexion -> cerrar();
    }

    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encargadoDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }
}

?>