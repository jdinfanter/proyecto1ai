<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/PrendaTallaDAO.php";

class PrendaTalla{
    private $prenda;
    private $talla;
    private $stock;
    private $conexion;
    private $prendatallaDAO;
    
    public function getPrenda(){
        return $this -> prenda;
    }
    
    public function getTalla(){
        return $this -> talla;
    }

    public function getStock(){
        return $this -> stock;
    }
   
        
    public function PrendaTalla($talla = "", $prenda = "", $stock=""){
        $this -> talla = $talla;
        $this -> prenda = $prenda;
        $this -> stock = $stock;
        $this -> conexion = new Conexion();
        $this -> prendatallaDAO = new PrendaTallaDAO($this -> talla, $this -> prenda, $this -> stock);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> prendatallaDAO -> consultar());
        $tallas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new PrendaTalla($resultado[1],"" , $resultado[0]);
            array_push($tallas, $t);
        }
        $this -> conexion -> cerrar();        
        return $tallas;
   
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tallaDAO -> consultarTodos());
        $tallas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Talla($resultado[0], $resultado[1]);
            array_push($tallas, $t);
        }
        $this -> conexion -> cerrar();        
        return $tallas;
    }
    
    public function consultarStock(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> prendatallaDAO -> consultarStock());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> stock = $resultado[0];
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> prendatallaDAO -> insertar());       
        $this -> conexion -> cerrar();    
    }
    
    public function actualizarStock($nuevostock){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->prendatallaDAO->actualizarStock($nuevostock));
        $this -> conexion -> cerrar();
    }

    

}

?>