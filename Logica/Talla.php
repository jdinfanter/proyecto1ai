<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/TallaDAO.php";

class Talla{
    private $idTalla;
    private $talla;
    private $conexion;
    private $tallaDAO;
    
    public function getIdTalla(){
        return $this -> idTalla;
    }
    
    public function getTalla(){
        return $this -> talla;
    }
   
        
    public function Talla($idTalla = "", $talla = ""){
        $this -> idTalla = $idTalla;
        $this -> talla = $talla;
        $this -> conexion = new Conexion();
        $this -> tallaDAO = new TallaDAO($this -> idTalla, $this -> talla);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tallaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idTipo = $resultado[0];
        $this -> tipo = $resultado[1];
   
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tallaDAO -> consultarTodos());
        $tallas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Talla($resultado[0], $resultado[1]);
            array_push($tallas, $t);
        }
        $this -> conexion -> cerrar();        
        return $tallas;
    }

    public function consultarTallas($idPrenda){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tallaDAO -> consultarTallas($idPrenda));
        $tallas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Talla($resultado[0], $resultado[1]);
            array_push($tallas, $t);
        }
        $this -> conexion -> cerrar();        
        return $tallas;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> tallaDAO -> insertar());        
        $this -> conexion -> cerrar();    
    }
    

}

?>