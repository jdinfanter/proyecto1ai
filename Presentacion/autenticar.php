

<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "", $correo, $clave);
$encargado= new Encargado("", "", "", $correo, $clave);
if($administrador -> autenticar()){
    $_SESSION["id"] = $administrador -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    $_SESSION["userName"] = $administrador -> getNombre()." ".$administrador -> getApellido();
    $log = new Log("", "Ingreso al sistema", "Rol: Administrador", date("yy-m-d"), date("g:i a"), $administrador -> getNombre()." ".$administrador -> getApellido());
    $log -> insertar();
    header("Location: index.php?pid=" . base64_encode("Presentacion/sesionAdministrador.php"));
}else if($cliente -> autenticar()){
       if($cliente -> getEstado() == -1){
        header("Location: index.php?pid=" . base64_encode("Presentacion/inicio.php")."&error=4");
    }else if($cliente -> getEstado() == 0){
        header("Location: index.php?pid=" . base64_encode("Presentacion/inicio.php")."&error=3");
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";
        $_SESSION["userName"] = $cliente -> getNombre()." ".$cliente -> getApellido();
        header("Location: index.php?pid=" . base64_encode("Presentacion/tienda.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Cliente", date("yy-m-d"), date("g:i a"), $cliente -> getNombre()." ".$cliente -> getApellido());
        $log -> insertar();
    }
}else if($encargado -> autenticar()){
    if($encargado -> getEstado() == -1){
        header("Location: index.php?pid=" . base64_encode("Presentacion/inicio.php")."&error=4");
    }else if($encargado -> getEstado() == 0){
        header("Location: index.php?pid=" . base64_encode("Presentacion/inicio.php")."&error=3");
    }else{
        $_SESSION["id"] = $encargado -> getIdEncargado();
        $_SESSION["rol"] = "Encargado";
        $_SESSION["userName"] = $encargado -> getNombre()." ".$encargado -> getApellido();
        header("Location: index.php?pid=" . base64_encode("Presentacion/sesionEncargado.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Encargado", date("yy-m-d"), date("g:i a"), $encargado -> getNombre()." ".$encargado -> getApellido());
        $log -> insertar();
    }
}else{
    $_SESSION["id"]="";
     header("Location: index.php?pid=" . base64_encode("Presentacion/inicio.php")."&error=2");
}

?>