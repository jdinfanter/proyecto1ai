<?php
$tipo = new Tipo("", "");
$tipos = $tipo -> consultarTodos();
?>


<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
  <a class="navbar-brand" href="#">Tienda</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentasion/tienda.php") ?>">Inicio</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Hombre
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php
            foreach($tipos as $t){
              echo "<a class='dropdown-item' href='index.php?pid=".base64_encode("Presentasion/tienda.php")."&filtro=".$t->getTipo()."&filtro2=hombre'>".$t->getTipo()."</a>";
            }
          ?>
        
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Mujer
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php
            foreach($tipos as $t){
              echo "<a class='dropdown-item' href='index.php?pid=".base64_encode("Presentasion/tienda.php")."&filtro=".$t->getTipo()."&filtro2=mujer'>".$t->getTipo()."</a>";
            }
          ?>
        
        </div>
      </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/InsertarCliente.php") ?>"><span class="fas fa-user"></span> Registro</a>
      </li>
      <?php if($_SESSION["rol"]== "") {?>
      <li class="nav-item">
         <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/inicio.php") ?>"><span class="fas fa-sign-in-alt"></span> Iniciar sesion</a>
      </li>
      <?php } else if($_SESSION["id"]!="" and $_SESSION["rol"]== "Cliente") {?>
      <li class="nav-item">
         <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
      <?php }?>
    </ul>
  </div>
</nav>