<?php 
$nombre = "";
$apellido = "";
$correo = "";
$clave = "";
$clave2 = "";
$foto = "";

if (isset($_POST["registrar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $clave2 = $_POST["clave2"];
    $error = "";
    $cliente = new Cliente("",$nombre, $apellido,$correo,$clave,"","");
    if(!$cliente  ->existeCorreo())
    {
        if($clave==$clave2)
        {
            
            if ($_FILES["imagen"]["name"] != "") {
                $rutaLocal = $_FILES["imagen"]["tmp_name"];
                $tipo_imagen = $_FILES["imagen"]["type"];
                $tiempo = new DateTime();
                $rutaRemota = "Imagenes_Clientes/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
                copy($rutaLocal, $rutaRemota);
                $cliente ->consultar();
                if($cliente -> getFoto() != ""){
                    unlink($cliente -> getFoto());
                }
                $cliente = new  Cliente("", $nombre, $apellido, $correo, $clave,$rutaRemota,"");
                $cliente  ->registrar();
            } else {
                $cliente->registrar();
			}
			$datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo."; imagen: ".$foto;
			$log = new Log("", "Registro cliente", $datosLog, date("yy-m-d"), date("g:i a"), $nombre." ".$apellido);
			$log -> insertar();
        }else {
            $error=1;
        }
    }else {
        $error=2;
       
    }
}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registro Cliente</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["registrar"])){ 
					if($error==1)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'La clave no coincide';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else if($error ==2)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'el correo ya existe';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else {
					?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Cliente registrado con exito
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }} ?>
					<form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/InsertarCliente.php")?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="email" name="correo" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Clave</label> 
							<input type="password" name="clave" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Confirmar clave</label> 
							<input type="password" name="clave2" class="form-control"  required>
						</div>
						 <div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						<button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>