<?php
if (isset($_POST["modificar"])) {
	$nombre = $_POST["nombre"];
	$apellido = $_POST["apellido"];
	$correo = $_POST["correo"];
	if($_SESSION["rol"]=="Administrador")
	{
	    $estado = $_POST["estado"];
	}
	if ($_FILES["imagen"]["name"] != "") {
		$rutaLocal = $_FILES["imagen"]["tmp_name"];
		$tipo_imagen = $_FILES["imagen"]["type"];
		$tiempo = new DateTime();
		$rutaRemota = "Imagenes_Clientes/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
		copy($rutaLocal, $rutaRemota);
		$cliente = new Cliente($_GET["idCliente"]);
		$cliente->consultar();
		if ($cliente->getFoto() != "") {
			unlink($cliente->getFoto());
		}
		if($_SESSION["rol"]=="Administrador")
		$cliente = new Cliente($_GET["idCliente"], $nombre, $apellido, $correo, "", $rutaRemota, $estado);
		else {
		    $estado_Actual= $cliente -> getEstado();
		    $cliente = new Cliente($_GET["idCliente"], $nombre, $apellido, $correo, "", $rutaRemota, $estado_Actual);
		}
		    
		$cliente->editar();
	} else {
		$clienteaux = new Cliente($_GET["idCliente"]);
		$clienteaux->consultar();
		$foto_Actual = $clienteaux->getFoto();
		$estado_Actual= $clienteaux -> getEstado();
		if($_SESSION["rol"]=="Administrador")
		    $cliente = new Cliente($_GET["idCliente"], $nombre, $apellido, $correo, "", $foto_Actual, $estado);
		else 
		    $cliente = new Cliente($_GET["idCliente"], $nombre, $apellido, $correo, "", $foto_Actual, $estado_Actual);
		$cliente->editar();
	}
	$userLog = "";
	if ($_SESSION["rol"] == "Cliente")
		$userLog = $nombre . " " . $apellido;
	else
		$userLog = $_SESSION["userName"];
	if($_SESSION["rol"]=="Administrador")
	   $datosLog = "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Estado: " . $estado;
	else
	    $datosLog = "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo;
	$log = new Log("", "Modificar cliente", $datosLog, date("yy-m-d"), date("g:i a"), $userLog);
	$log->insertar();
} else {
	$cliente = new Cliente($_GET["idCliente"]);
	$cliente->consultar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Cliente</h4>
				</div>
				<div class="card-body">
					<?php if (isset($_POST["modificar"])) { ?>
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							Datos editados
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/modificarCliente.php") ?>&idCliente=<?php echo $_GET["idCliente"] ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $cliente->getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label>
							<input type="text" name="apellido" class="form-control" min="1" value="<?php echo $cliente->getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label>
							<input type="email" name="correo" class="form-control" min="1" value="<?php echo $cliente->getCorreo() ?>" required>
						</div>
						<?php if($_SESSION["rol"]=="Administrador"){?>
						<div class="form-group">
							<label>Estado</label>
							<input type="number" name="estado" class="form-control" min="0" value="<?php echo $cliente->getEstado() ?>" required>
						</div>
						<?php }?>
						<div class="form-group">
							<label>Imagen</label>
							<input type="file" name="imagen" class="form-control">
						</div>
						<button type="submit" name="modificar" class="btn btn-dark">Modificar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>