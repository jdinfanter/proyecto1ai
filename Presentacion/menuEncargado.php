<?php
$encargado = new  Encargado($_SESSION["id"]);
$encargado ->consultar();
?>
<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionEncargado.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Prenda
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Prenda/crearPrenda.php")?>">Registrar prendas</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Prenda/consultarPrendaPagina.php")?>">Consultar Todas Las Prendas</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Prenda/Inventario.php")?>">Manejo De Inventario</a>
        </div>
      </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Encargado: <?php echo $encargado-> getNombre() ?> <?php echo $encargado-> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Encargado/modificarEncargado.php")?>&idEncargado=<?php echo $_SESSION["id"]?>" method="post""">Editar Perfil</a> 
						<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Encargado/Encargado_cambiar_clave.php")?>&idEncargado=<?php echo $_SESSION["id"]?>" method="post"">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>