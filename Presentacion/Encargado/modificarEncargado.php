<?php
if(isset($_POST["modificar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    if($_SESSION["rol"]=="Administrador")
    {
    $estado = $_POST["estado"];
    }
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo_imagen = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes_Encargados/" . $tiempo -> getTimestamp() . (($tipo_imagen == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $encargado = new Encargado($_GET["idEncargado"]);
        $encargado->consultar();
        if($encargado -> getFoto() != ""){
            unlink($encargado -> getFoto());
        }
        if($_SESSION["rol"]=="Administrador")
        $encargado = new Encargado($_GET["idEncargado"], $nombre,$apellido, $correo,"",$rutaRemota,$estado);
        else {
            $estado_Actual= $encargado -> getEstado();
            $encargado = new Encargado($_GET["idEncargado"], $nombre,$apellido, $correo,"",$rutaRemota,$estado_Actual);
            
        }
        $encargado -> editar();
    }else {
        $encargadoaux = new Encargado($_GET["idEncargado"]);
        $encargadoaux->consultar();
        $foto_Actual= $encargadoaux -> getFoto();
        $estado_Actual= $encargadoaux -> getEstado();
        if($_SESSION["rol"]=="Administrador")
            
            $encargado= new Encargado($_GET["idEncargado"], $nombre,$apellido, $correo,"", $foto_Actual,$estado);
        else 
            $encargado= new Encargado($_GET["idEncargado"], $nombre,$apellido, $correo,"", $foto_Actual,$estado_Actual);
            $encargado -> editar();
	}
	$userLog = "";
	if ($_SESSION["rol"] == "Encargado")
		$userLog = $nombre . " " . $apellido;
	else
		$userLog = $_SESSION["userName"];
    if($_SESSION["rol"]=="Administrador")
	   $datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo."; estado:".$estado;
    else 
        $datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo;
	$log = new Log("", "Modificar encargado", $datosLog, date("yy-m-d"), date("g:i a"), $userLog);
	$log -> insertar();
}else{
    $encargado = new Encargado($_GET["idEncargado"]);
    $encargado-> consultar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Encargado</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["modificar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("Presentacion/Encargado/modificarEncargado.php") ?>&idEncargado=<?php echo $_GET["idEncargado"]?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $encargado -> getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control" min="1" value="<?php echo $encargado -> getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="email" name="correo" class="form-control" min="1" value="<?php echo $encargado -> getCorreo() ?>" required>
						</div>
						<?php if (!$_SESSION["rol"]=="Encargado"){} else if($_SESSION["rol"]=="Administrador"){ ?>
						<div class="form-group">
							<label>Estado</label> 
							<input type="number" name="estado" class="form-control" min="0" value="<?php echo $encargado -> getEstado() ?>" required>
						</div>
						<?php }?>
						<div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						
						<button type="submit" name="modificar" class="btn btn-dark">Modificar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>