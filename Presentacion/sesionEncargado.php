<?php
$encargado = new Encargado($_SESSION["id"]);
$encargado -> consultar();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Bienvenido Encargado</h4>
				</div>
              	<div class="card-body">
              		<div class="row">
              			<div class="col-3">
              				<img src="<?php echo ($encargado -> getFoto() != "")?$encargado -> getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user2-2-icon.png"; ?>" width="100%" class="img-thumbnail">              			
              			</div>
              			<div class="col-9">
							<table class="table table-hover">
								<tr>
									<th>Nombre</th>
									<td><?php echo $encargado -> getNombre() ?></td>
								</tr>
								<tr>
									<th>Apellido</th>
									<td><?php echo $encargado -> getApellido() ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $encargado -> getCorreo() ?></td>
								</tr>
								<tr>
									<th>Estado</th>
									<td>
									<?php 
									if ($encargado -> getEstado()==1)
								        echo "Activo";
									else if ($encargado -> getEstado()==0)
									    echo "Deshabilidato"
									?></td>
								</tr>
							</table>
						</div>              		
              		</div>              	
            	</div>
            </div>
		</div>
	</div>
</div>