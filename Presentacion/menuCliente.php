<?php
$cliente = new  Cliente($_SESSION["id"]);
$cliente->consultar();

$tipo = new Tipo("", "");
$tipos = $tipo -> consultarTodos();

?>
<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("Presentacion/tienda.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
  <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/tienda.php") ?>">Inicio</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Hombre
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php
            foreach($tipos as $t){
              echo "<a class='dropdown-item' href='index.php?pid=".base64_encode("Presentacion/tienda.php")."&filtro=".$t->getTipo()."&filtro2=hombre'>".$t->getTipo()."</a>";
            }
          ?>
        
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Mujer
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php
            foreach($tipos as $t){
              echo "<a class='dropdown-item' href='index.php?pid=".base64_encode("Presentacion/tienda.php")."&filtro=".$t->getTipo()."&filtro2=mujer'>".$t->getTipo()."</a>";
            }
          ?>
        
        </div>
      </li>
    </ul>


    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Cliente: <?php echo $cliente-> getNombre() ?> <?php echo $cliente-> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/sesionCliente.php") ?>">Ver Perfil</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/modificarCliente.php") ?>&idCliente=<?php echo $_SESSION["id"]?>" method="post">Editar Perfil</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/cliente_cambiar_clave.php") ?>&idCliente=<?php echo $_SESSION["id"]?>" method="post">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>