<?php

$ventaprenda = new VentaPrenda();
$ventaprendas = $ventaprenda->consultarArray($_GET["idVenta"]);
$comprar = $_GET["comprar"];


if (isset($_GET["detalles"])) {
} else {
    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>";
    echo "Compra exitosa";
    echo "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
    echo "</div>";
}
?>

<div class="container">
    <div class="row justify-content-md-center">
        
        <div class="col-12 col-md-8 m-2">
            <center>
            <div id="cabeceraFactura" >
                <label>NOMBRE: <?php echo $ventaprendas[0]->getNombreCliente() . " " . $ventaprendas[0]->getApellidoCliente() ?></label><br>
                <label>FECHA: <?php echo $ventaprendas[0]->getFechaVenta() ?></label><br>
            </div>
            <table id="detalle" border="2" width="2" cellspacing="1" cellpadding="1" class="m-0 table">
                <thead class="bg-info">
                    <tr>
                        <th>Prenda</th>
                        <th>Talla</th>
                        <th>Cantidad</th>
                        <th>Valor Unitario</th>
                        <th>Valor Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ventaprendas as $ventaprendaActual) { ?>
                        <tr>
                            <td><?php echo $ventaprendaActual->getNombrePrenda(); ?></td>
                            <td><?php echo $ventaprendaActual->getTallaPrenda(); ?></td>
                            <td><?php echo $ventaprendaActual->getCantidad(); ?></td>
                            <td><?php echo "$".$ventaprendaActual->getPrecio(); ?></td>
                            <td><?php echo "$".($ventaprendaActual->getCantidad() * $ventaprendaActual->getPrecio()); ?></td>
                        </tr>
                        <?php 
                        if($comprar==1)
                        {  
                            $pt = new PrendaTalla ($ventaprendaActual->getIdTalla(),$ventaprendaActual->getIdPrenda(),"");
                            $pt -> consultarStock(); 
                            $nuevostock = ($pt ->getStock() - $ventaprendaActual->getCantidad());
                            $pt -> actualizarStock($nuevostock); 
                        }
                        ?>
                    <?php } ?>

                    <tr>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                    </tr>
                    <tr>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                    </tr>
                    <tr>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                    </tr>
                    <tr>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                    </tr>
                    <tr>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                    </tr>
                    <tr>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                        <td><br> </td>
                    </tr>
                    <tr>
                        <th colspan="4"> Total a pagar</th>
                        <td><?php echo "$".$ventaprendas[0]->getPrecioTotal() ?> </td>
                    </tr>
                </tbody>
            </table>

            </center>

        </div>
    </div>
</div>