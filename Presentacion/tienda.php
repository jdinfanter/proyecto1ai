<?php
//carro

if (isset($_POST["comprar"])) {
    if ($_SESSION["rol"] == "") {
        header("Location: index.php?pid=" . base64_encode("Presentacion/inicio.php") . "&error=1");
    } else if ($_SESSION["rol"] == "Cliente") {
        $fecha = date("yy-m-d");
        $cliente = $_SESSION["id"];
        $total = 0;
        $idVenta;
        if (!empty($_SESSION["add_carro"])) {
            foreach ($_SESSION["add_carro"] as $keys => $values) {
                $total = $total + ($values["item_cantidad"] * $values["item_precio"]);
            }
            $venta = new Venta("", $fecha, $total, $cliente, "");
            $idVenta =  $venta->insertar();
            foreach ($_SESSION["add_carro"] as $keys => $values) {
                $ventaprenda = new VentaPrenda("", "", "", "", "", "", $idVenta, $values["item_id"], $values["item_talla"], $values["item_cantidad"], $values["item_precio"]);
                $ventaprenda->insertar();
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
                echo 'Compra exitosa';
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                echo '</div>';
                header("Location: index.php?pid=" . base64_encode("Presentacion/Factura.php") . "&idVenta=" . $idVenta . "&comprar=1");
                unset($_SESSION['add_carro'][$keys]);
            }
        } else if (empty($_SESSION["add_carro"])) {
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
            echo 'No hay productos seleccionados';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            echo '</div>';
        }
        $datosLog = "Numero de factura: ".$idVenta."; precio total: ".$total;
        $log = new Log("", "Registro venta", $datosLog, date("yy-m-d"), date("g:i a"), $_SESSION["userName"]);
        $log -> insertar();
    }
}

if (isset($_POST['agregar'])) {
    if (isset($_SESSION['add_carro'])) {
        $item_array_id_cart = array_column($_SESSION['add_carro'], 'item_id');
        if (!in_array($_GET['id'], $item_array_id_cart)) {
            $count = count($_SESSION['add_carro']);
            $item_array = array(
                'item_id'        => $_GET['id'],
                'item_nombre'    => $_POST['hidden_nombre'],
                'item_precio'    => $_POST['hidden_precio'],
                'item_cantidad'  => $_POST['cantidad'],
                'item_talla'  => $_POST['selectTalla'],
            );

            $_SESSION['add_carro'][$count] = $item_array;
        } else {
            echo '<script>alert("El Producto ya existe!");</script>';
        }
    } else {
        $item_array = array(
            'item_id'        => $_GET['id'],
            'item_nombre'    => $_POST['hidden_nombre'],
            'item_precio'    => $_POST['hidden_precio'],
            'item_cantidad'  => $_POST['cantidad'],
            'item_talla'  => $_POST['selectTalla'],
        );

        $_SESSION['add_carro'][0] = $item_array;
    }
}
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') {
        foreach ($_SESSION['add_carro'] as $key => $value) {
            if ($value['item_id'] == $_GET['id']) {
                unset($_SESSION['add_carro'][$key]);
                echo '<script>window.location="index.php?pid=' . base64_encode("Presentacion/tienda.php") . ' ";</script>';
            }
        }
    }
}


//paginacion
require_once "logica/Prenda.php";
$prenda = new Prenda("", "", "", "", "", "");
$cantidad = 15;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}

if (isset($_GET["filtro"])  && isset($_GET["filtro2"])) {
    $filtro = $_GET["filtro"];
    $filtro2 = $_GET["filtro2"];
} else {
    $filtro = "";
    $filtro2 = "";
}

$prendas = $prenda->consultarFiltro($cantidad, $pagina, $filtro, $filtro2);
$totalRegistros = $prenda->consultarCantidad();
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);

?>



<!-- detalles del carro -->
<div class="container-lg mt-3">
    <button><img src="https://icons.iconarchive.com/icons/fasticon/shop-cart/128/shop-cart-icon.png" class="img-responsive" width="40px" type="button" id="btnCarrit" class="btn btn-info"></button></img>
    <div class="table-responsive" id="carrit" style="display: none;">
        <h3>Detalle de la Orden</h3>
        <table class="table table-bordered">
            <tr>
                <th width="40%">Nombre</th>
                <th width="10%">Precio</th>
                <th width="20%">Cantidad</th>
                <th width="15%">Total</th>
                <th width="5%"></th>
            </tr>
            <?php
            if (!empty($_SESSION["add_carro"])) {
                $total = 0;
                foreach ($_SESSION["add_carro"] as $keys => $values) {
            ?>
                    <tr>
                        <td><?php echo $values["item_nombre"]; ?></td>
                        <td><?php echo $values["item_precio"]; ?></td>
                        <td> <?php echo $values["item_cantidad"]; ?></td>
                        <td>$ <?php echo number_format($values["item_cantidad"] * $values["item_precio"], 2); ?></td>
                        <td><a href="index.php?pid=<?php echo base64_encode("Presentacion/tienda.php") ?>&action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Remover</span></a></td>
                    </tr>
                <?php
                    $total = $total + ($values["item_cantidad"] * $values["item_precio"]);
                }
                ?>
                <tr>
                    <td colspan="3" align="right">Total</td>
                    <td align="right">$ <?php echo number_format($total, 2); ?></td>
                    <td></td>
                </tr>
            <?php
            } else {
            ?>
                <tr>
                    <td colspan="4" style="color: red" align="center"><strong>No hay Producto Agregado!</strong></td>
                </tr>
            <?php
            }
            ?>
        </table>
        <form method="post" action="index.php?pid=<?php echo base64_encode("Presentacion/tienda.php") ?>">
            <button type="submit" name="comprar" class='btn btn-xs btn-primary'>Comprar</button>
        </form>
    </div>

    <!--despliegue de productos -->
    <div class="">
        <div class="text-right mr-3">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($prendas) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
        <div class="card-body">
            <div class="row">
                <?php

                if ($totalRegistros > 0) {
                    foreach ($prendas as $prendaActual) {
                ?>
                        <div class="col-md-4 p-1">
                            <form  method="post" action="index.php?pid=<?php echo base64_encode("Presentacion/tienda.php") ?>&action=add&id=<?php echo $prendaActual->getIdPrenda(); ?>">
                                <?php

                                ?>
                                <div class="card p-2 ">
                                    <div class="text-center">
                                        <img src="<?php echo ($prendaActual->getFoto() != "") ?  $prendaActual->getFoto() : "http://icons.iconarchive.com/icons/iconsmind/outline/512/Full-Cart-icon.png"; ?>" class="img-responsive" height="160px" /><br />
                                    </div>

                                    <h4 class="text-dark"><?php echo $prendaActual->getNombre(); ?></h4>
                                    <span class="font-italic"><?php echo $prendaActual->getDescripcion(); ?></span>
                                    <h6 class="text-dark">$<?php echo $prendaActual->getPrecio(); ?></h6>

                                    <input type="hidden" name="hidden_nombre" value="<?php echo $prendaActual->getNombre(); ?>" />
                                    <input type="hidden" name="hidden_descripcion" value="<?php echo $prendaActual->getDescripcion(); ?>" />
                                    <input type="hidden" name="hidden_precio" value="<?php echo $prendaActual->getPrecio(); ?>" />


                                    <!-- Button trigger modal -->
                                    <?php echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#m" . $prendaActual->getIdPrenda() . "'>ver</button> " ?>



                                    <!-- Modal -->
                                    <?php echo "<div class='modal fade' id='m" . $prendaActual->getIdPrenda() . "' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>" ?>
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <?php echo "<h5 class='modal-title' id='exampleModalLongTitle'>" . $prendaActual->getNombre() . "</h5>" ?>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body ">

                                                <?php echo "<div class='mb-2 text-center'><img src='" . $prendaActual->getFoto() . "' class='img-responsive' width='300px'></div>";
                                                echo "<div >Cantidad: <input type='text' name='cantidad' class='mb-2' value='1' whith='200px'/></br>";
                                                echo "Seleccione una talla:  ";
                                                $talla = new Talla();
                                                $tallas = $talla->consultarTallas($prendaActual->getIdPrenda());
                                                echo "<select name='selectTalla'>";
                                                foreach ($tallas as $t) {
                                                    echo "<option value='" . $t->getIdTalla() . "' class='btn btn-outline-secondary'>" . $t->getTalla() . "</button>";
                                                }
                                                echo "</select></div>";
                                                ?>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                <input type="submit" name="agregar" style="margin-top:5px;" class="btn btn-primary" value="Añadir al carro" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        </form>
            </div>
    <?php
                    }
                }
    ?>
        </div>


        <!-- paginacion html -->
        <div class="text-center">
            <nav  class="table-responsive mb-2">
                <ul class="pagination">
                    <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/prenda/consultarPrendaPagina.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                    <?php
                    for ($i = 1; $i <= $totalPaginas; $i++) {
                        if ($i == $pagina) {
                            echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                        } else {
                            echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/tienda.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                        }
                    }
                    ?>
                    <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/prenda/consultarPrendaPagina.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                </ul>
            </nav>
        </div>
        <select id="cantidad">
            <option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
            <option value="30" <?php echo ($cantidad == 30) ? "selected" : "" ?>>30</option>
            <option value="45" <?php echo ($cantidad == 45) ? "selected" : "" ?>>45</option>
            <option value="60" <?php echo ($cantidad == 60) ? "selected" : "" ?>>60</option>
        </select>
    </div>
</div>
</div>

</div>

<script>
$("document").ready(function(){
    $("#cantidad").on("change", function() {
        url = "index.php?pid=<?php echo base64_encode("Presentacion/tienda.php") ?>&cantidad=" + $(this).val();
        location.replace(url);
    });
    
    $("#btnCarrit").click(function(){
        var carr = $("#carrit");
        if(carr.is(":visible")){
            carr.css("display","none");
        }else{
            carr.css("display","block");
        }
    })
})
    
</script>