<?php
if(isset($_POST["modificar"])){
    if ($_FILES["imagen"]["name"] != "") {
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo_imagen = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagen_Admin/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
        copy($rutaLocal, $rutaRemota);
        $admin = new Administrador($_GET["idAdministrador"]);
        $admin ->consultar();
        if ($admin->getFoto() != "") {
            unlink($admin->getFoto());
        }
        $administrador = new Administrador($_GET["idAdministrador"], $_POST["nombre"], $_POST["apellido"], $_POST["correo"],"",$rutaRemota);
        $administrador -> editar();
    }else {
        $adminaux = new Administrador($_GET["idAdministrador"]);
        $adminaux ->consultar();
        $foto_Actual = $adminaux ->getFoto();
        $administrador = new Administrador($_GET["idAdministrador"], $_POST["nombre"], $_POST["apellido"], $_POST["correo"],"",$foto_Actual);
        $administrador -> editar();
    }

	$datosLog = "Nombre: ".$_POST["nombre"]."; Apellido: ".$_POST["apellido"]."; Correo: ".$_POST["correo"];
	$log = new Log("", "Modificar Administrador", $datosLog, date("yy-m-d"), date("g:i a"), $_POST["nombre"]." ".$_POST["apellido"]);
	$log -> insertar();
}else{
    $administrador = new Administrador($_GET["idAdministrador"]);
    $administrador-> consultar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Administrador</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["modificar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Editar_administrador.php") ?>&idAdministrador=<?php echo $_GET["idAdministrador"]?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $administrador -> getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control" min="1" value="<?php echo $administrador -> getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="text" name="correo" class="form-control" min="1" value="<?php echo $administrador -> getCorreo() ?>" required>
						</div>
						<div class="form-group">
							<label>Imagen</label>
							<input type="file" name="imagen" class="form-control">
						</div>
						<button type="submit" name="modificar" class="btn btn-dark">Modificar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>