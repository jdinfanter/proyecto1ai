<?php
require_once "logica/Prenda.php";
$prenda = new Prenda("", "", "", "", "", "");
$cantidad = 5;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}


$prendas = $prenda->consultarPaginacion($cantidad, $pagina);
$totalRegistros = $prenda->consultarCantidad();
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container-lg mt-3">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header text-white bg-dark">
                    <h4>Inventario</h4>
                </div>
                <div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($prendas) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
                <div class="card-body">
                    <table class="table table-responsive-lg table-hover table-striped">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Precio</th>
                            <th>Descripcion</th>
                            <th>Tipo</th>
                            <th>Genero</th>
                            <th>Talla</th>
                            <th>Stock</th>
                            <th>Imagen</th>

                        </tr>
                        <?php
                        $i = 1;
                        foreach ($prendas as $prendaActual) {
                            echo "<tr>";
                            echo "<td>" . $i . "</td>";
                            echo "<td>" . $prendaActual->getNombre() . "</td>";
                            echo "<td>" . $prendaActual->getPrecio() . "</td>";
                            echo "<td>" . $prendaActual->getDescripcion() . "</td>";
                            echo "<td>" . $prendaActual->getTipo() . "</td>";
                            echo "<td>" . $prendaActual->getGenero() . "</td>";
                            $talla = new Talla();
                            $tallas = $talla->consultarTallas($prendaActual->getIdPrenda());
                            echo "<td><select id='combo" . $prendaActual->getIdPrenda() . "'name='selectTalla' data='" . $prendaActual->getIdPrenda() . "'>";
                            $tallaP ="";
                            foreach ($tallas as $t) {
                                echo "<option  value='" . $t->getIdTalla() . "' class='btn btn-outline-secondary'>" . $t->getTalla() . "</button>";
                                $tallaP = $tallas[0]->getIdTalla();
                            }
                            $tallaPrenda = new PrendaTalla( $tallaP, $prendaActual->getIdPrenda(), "");
                            $tallaPrenda->consultarStock();
                            echo "</select></td>";
                            echo "<td><a id='stock" .$prendaActual->getIdPrenda(). "'>" . $tallaPrenda->getStock() . "</a></td>";
                            echo "<td>" . (($prendaActual->getFoto() != "") ? "<img src='" . $prendaActual->getFoto() . "' height='80px'>" : "") . "</td>";
                            echo "</tr>";
                            $i++;
                        }
                        ?>
                    </table>
                    <div class="text-center">
                        <nav>
                            <ul class="pagination">
                                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/prenda/Inventario.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                                <?php
                                for ($i = 1; $i <= $totalPaginas; $i++) {
                                    if ($i == $pagina) {
                                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                                    } else {
                                        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Prenda/Inventario.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                                    }
                                }
                                ?>
                                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/prenda/Inventario.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                            </ul>
                        </nav>
                    </div>
                    <select id="cantidad">
                        <option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
                        <option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
                        <option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
                        <option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#cantidad").on("change", function() {
        url = "index.php?pid=<?php echo base64_encode("Presentacion/Prenda/Inventario.php") ?>&cantidad=" + $(this).val();
        location.replace(url);
    });

    <?php foreach ($prendas as $prendaActual) { ?>
        $("#combo<?php echo $prendaActual->getIdPrenda() ?>").on("change", function() {
            var idTalla = $(this).val();
            var idPrenda = $(this).attr("data");
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/Prenda/inventarioAjax.php") ?>&idPrenda=" + idPrenda + "&idTalla=" + idTalla;
            $("#stock" + idPrenda).load(url);

        });
    <?php } ?>
</script>