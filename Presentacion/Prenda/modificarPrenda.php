<?php
$nombre = "";
$precio = "";
$descripcion = "";
$foto = "";
$tipo = "";
$genero = "";
$tipo = new Tipo("", "");
$genero = new Genero("", "");
$talla = new Talla("", "");
$tipos = $tipo->consultarTodos();
$generos = $genero->consultarTodos();
$tallas = $talla->consultarTodos();
$tallasSeleccionadas = array();

if (isset($_POST["modificar"])) {
    if (isset($_POST["modificar"])) {
        $nombre = $_POST["nombre"];
        $precio = $_POST["precio"];
        $descripcion = $_POST["descripcion"];
        //$foto = $_POST["imagen"];
        $tipoNombre = $_POST["tipo"];
        $generoNombre = $_POST["genero"];
        foreach ($tipos as $tActual) {
            if ($tActual->getTipo() == $tipoNombre) {
                $tipo = $tActual->getIdTipo();
            };
        }
        foreach ($generos as $gActual) {
            if ($gActual->getGenero() == $generoNombre) {
                $genero = $gActual->getIdGenero();
            };
        }
        if($_FILES["imagen"]["name"] != ""){
            $rutaLocal = $_FILES["imagen"]["tmp_name"];
            $tipo_imagen = $_FILES["imagen"]["type"];
            $tiempo = new DateTime();
            $rutaRemota = "Imagenes_prendas/" . $tiempo -> getTimestamp() . (($tipo_imagen == "image/png")?".png":".jpg");
            copy($rutaLocal,$rutaRemota);
            $prenda = new Prenda($_GET["idPrenda"]);
            $prenda->consultar();
            if($prenda -> getFoto() != ""){
                unlink($prenda -> getFoto());
            }
            $prenda = new Prenda($_GET["idPrenda"], $nombre, $precio, $descripcion, $rutaRemota, $tipo, $genero);
            $prenda -> editar();
        }else {
            $prenda1 = new Prenda($_GET["idPrenda"]);
            $prenda1->consultar();
            $foto_Actual= $prenda1->getFoto();
            $prenda = new Prenda($_GET["idPrenda"], $nombre, $precio, $descripcion,$foto_Actual, $tipo, $genero);
            $prenda -> editar();
        }
        $datosLog = "nombre: ".$nombre."; precio: ".$precio."; Descripcion ".$descripcion."; tipo:".$tipoNombre."; genero: ".$generoNombre. "; foto: ".$foto;
        $log = new Log("", "Modificar prenda", $datosLog, date("yy-m-d"), date("g:i a"), $_SESSION["userName"]);
        $log -> insertar();
    }
    
} else {
    $prenda = new Prenda($_GET["idPrenda"]);
    $prenda->consultar();
}

?>

<div class="container-lg">
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-sm-12 mt-4">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    <h4>Modificar Prenda</h4>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/prenda/modificarPrenda.php") ?>&idPrenda=<?php echo $_GET["idPrenda"]?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="Nombre">Nombre de la prenda</label>
                            <input type="text" class="form-control form-control-sm" name="nombre" value="<?php echo $prenda->getNombre() ?>" required="">
                        </div>
                        <div class="form-group">
                            <label for="Precio">Precio</label>
                            <input type="number" class="form-control form-control-sm" name="precio" value="<?php echo $prenda->getPrecio() ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción o detalles</label>
                            <input type="text" class="form-control form-control-sm" name="descripcion" value="<?php echo $prenda->getDescripcion() ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="Tipo">Tipo de prenda</label><br>
                            <select id="select-tipo" name="tipo" value="<?php echo $prenda->getTipo() ?>" required>
                                <?php
                                echo "<option>";
                                echo ($prenda->getTipo()>0)?"":"<a class='dropdown-item' href='#'>" . $prenda->getTipo() . "</a>";
                                echo "</option>";
                                foreach ($tipos as $tipoActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $tipoActual->getTipo() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Tipo">Genero</label><br>
                            <select id="select-genero" name="genero" value="<?php echo $prenda->getGenero() ?>" required>
                                <?php
                                echo "<option>";
                                echo ($prenda->getGenero()>0)?"":"<a class='dropdown-item' href='#'>" . $prenda->getGenero() . "</a>";
                                echo "</option>";
                                foreach ($generos as $generoActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $generoActual->getGenero() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <label for="tallas">Establecer tallas disponibles para la prenda</label><br>
                            <?php
                            foreach ($tallas as $tallaActual) {
                                echo "<label >" . $tallaActual->getTalla() . "</label>";
                                echo "<input type='checkbox' name=" . $tallaActual->getTalla() . " value=" . $tallaActual->getTalla() . ">" . ", ";
                            }
                            ?>

                        </div> -->
                        <div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
                        <button type="submit" name="modificar" class="btn btn-dark">Modificar</button>
                    </form>
                    <?php if (isset($_POST["modificar"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Datos ingresados
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    <?php } ?>
                </div>
            </div>


        </div>
    </div>
</div>