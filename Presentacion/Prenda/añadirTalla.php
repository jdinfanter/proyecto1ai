<?php

if (isset($_POST["modificarstock"])) {
    $talla = new Talla("", "");
    $tallas = $talla->consultarTodos();
    foreach ($tallas as $taA) {
        if (isset($_POST["hidden_talla" . $taA->getTalla()])) {
            $stock = $_POST["stock" . $taA->getTalla()];
            $talla = $_POST["hidden_talla" . $taA->getTalla()];
            $PT = new PrendaTalla($taA->getIdTalla(), $_GET["idPrenda"], $stock);
            $PT->actualizarStock($stock);
        }
    }
}

$idPrenda = $_GET["idPrenda"];
$tallaConsulta = new Talla("","");
$tallasCon = $tallaConsulta -> consultarTodos();

if (isset($_POST["añadirTalla"])) {
    $selectTalla = $_POST["talla"];
    $stockN = $_POST["stockN"];
    foreach ($tallasCon as $tallaA) {
        if ($tallaA->getTalla() == $selectTalla) {
            $tallaselecionada = $tallaA->getIdTalla();
        };
    }
    $prendatalla = new PrendaTalla($tallaselecionada , $idPrenda, $stockN);
    $prendatalla -> insertar();
}

$tallaP = new PrendaTalla("", $idPrenda, "");
$tallasp = $tallaP->consultar();


?>


<div class="container-lg">
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-sm-12 mt-4">
            <?php if (isset($_POST["modificarstock"])) { ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    Datos ingresados
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            <?php } ?>
            <div class="card p-2">
                <h3>Modificar Stock:</h3>
                <form method="post" action="index.php?pid=<?php echo base64_encode("Presentacion/Prenda/añadirTalla.php") ?>&idPrenda=<?php echo $_GET["idPrenda"] ?>">
                
                    <?php
                    foreach ($tallasp as $tActual) {
                        echo "<input type='hidden' name='hidden_talla" . $tActual->getTalla() . "' value='" . $tActual->getTalla() . "' />";
                        echo "<h5>Talla: <strong>" . $tActual->getTalla() . "</strong></h5><p>Stock: <input type='number' name='stock" . $tActual->getTalla() . "' value='" . $tActual->getStock() . "'></p></br>";
                    }
                    ?>
                    <input type="submit" name="modificarstock" class="btn btn-dark" value="Modificar">
                </form>
            </div>

        </div>

        <div class="col-md-6 col-sm-12 mt-4">
        <?php if (isset($_POST["añadirTalla"])) { ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    Datos ingresados
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            <?php } ?>
            <div class="card p-2">
                <h3>Añadir talla:</h3>
                <form method="post" action="index.php?pid=<?php echo base64_encode("Presentacion/Prenda/añadirTalla.php") ?>&idPrenda=<?php echo $_GET["idPrenda"] ?>">
                <label for="Precio">Seleccione Talla: </label>
                    <select id="select_talla" name="talla">
                        <?php
                        foreach ($tallasCon as $tallaAct) {
                            echo "<option>";
                            echo "<a class='dropdown-item' href='#'>" . $tallaAct->getTalla() . "</a>";
                            echo "</option>";
                        }
                        ?>
                    </select>
                    <div class="form-group">
                            <label for="Precio">Stock</label>
                            <input type="number" class="" name="stockN" required="">
                        </div>
                    <input type="submit" name="añadirTalla" class="btn btn-dark" value="Añadir">
                </form>
            </div>

        </div>
    </div>
</div>