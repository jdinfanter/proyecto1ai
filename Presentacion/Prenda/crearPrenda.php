<?php
$idPrenda = "";
$nombre = "";
$precio = "";
$descripcion = "";
$foto = "";
$tipo = "";
$genero = "";
$tipo = new Tipo("", "");
$genero = new Genero("", "");
$talla = new Talla("", "");
$tipos = $tipo->consultarTodos();
$generos = $genero->consultarTodos();
$tallas = $talla->consultarTodos();
$tallasSeleccionada = "";
if (isset($_POST["crear"])) {
    $nombre = $_POST["nombre"];
    $precio = $_POST["precio"];
    $descripcion = $_POST["descripcion"];
    $tipoNombre = $_POST["tipo"];
    $generoNombre = $_POST["genero"];
    $selectTalla = $_POST["talla"];
    $stock = $_POST["stock"];
    foreach ($tipos as $tActual) {
        if ($tActual->getTipo() == $tipoNombre) {
            $tipo = $tActual->getIdTipo();
        };
    }
    foreach ($generos as $gActual) {
        if ($gActual->getGenero() == $generoNombre) {
            $genero = $gActual->getIdGenero();
        };
    }
    foreach ($tallas as $tallaActual) {
        if ($tallaActual->getTalla() == $selectTalla) {
            $tallaselecionada = $tallaActual->getIdTalla();

        };
    }
    if ($_FILES["imagen"]["name"] != "") {
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo_imagen = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes_prendas/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
        copy($rutaLocal, $rutaRemota);
        // $prenda = new Prenda("");
        // $prenda->consultar();
        // if($prenda -> getFoto() != ""){
        //     unlink($prenda -> getFoto());
        // }
        $prenda = new Prenda("", $nombre, $precio, $descripcion, $rutaRemota, $tipo, $genero);
        $idPrenda =  $prenda->insertar();
    } else {
        $prenda = new Prenda("", $nombre, $precio, $descripcion, "", $tipo, $genero);
        $idPrenda = $prenda->insertar();
    }
    ///REGISTRAR LA CANTIDAD DE STOCK DE LA PRENDA Y SU RESPECTIVA TALLA
    $prendatalla = new PrendaTalla($tallaselecionada , $idPrenda, $stock);
    $prendatalla->insertar();
    $datosLog = "nombre: ".$nombre."; precio: ".$precio."; descripcion: ".$descripcion."; tipo:".$tipoNombre ."; talla: ".$selectTalla."; genero: ".$generoNombre."; foto: ".$foto;
    $log = new Log("", "Insertar prenda", $datosLog, date("yy-m-d"), date("g:i a"), $_SESSION["userName"]);
    $log -> insertar();
}

?>

<div class="container-lg">
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-sm-12 mt-4">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    <h4>Registrar Prenda</h4>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/prenda/crearPrenda.php") ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="Nombre">Nombre De La Prenda</label>
                            <input type="text" class="form-control form-control-sm" name="nombre" required="">
                        </div>
                        <div class="form-group">
                            <label for="Precio">Precio</label>
                            <input type="number" class="form-control form-control-sm" name="precio" required="">
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripcion o detalles</label>
                            <input type="text" class="form-control form-control-sm" name="descripcion">
                        </div>
                        <div class="form-group">
                            <label for="Tipo">Tipo de prenda</label><br>
                            <select id="select-tipo" name="tipo">
                                <?php
                                foreach ($tipos as $tipoActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $tipoActual->getTipo() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Genero">Genero</label><br>
                            <select id="select-genero" name="genero">
                                <?php
                                foreach ($generos as $generoActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $generoActual->getGenero() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tallas">Establecer tallas disponibles para la prenda</label><br>
                            <select id="select_talla" name="talla">
                                <?php
                                foreach ($tallas as $tallaActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $tallaActual->getTalla() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                        </div>
                         <div class="form-group">
                            <label for="Precio">Stock</label>
                            <input type="number" class="form-control form-control-sm" name="stock" required="">
                        </div>
                        <div class="form-group">
                            <label>Imagen</label>
                            <input type="file" name="imagen" class="form-control">
                        </div>
                        <button type="submit" name="crear" class="btn btn-dark">Registrar</button>
                    </form>
                    <?php if (isset($_POST["crear"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Datos ingresados
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    <?php } ?>
                </div>
            </div>


        </div>
    </div>
</div>