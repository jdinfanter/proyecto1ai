<?php
if(isset($_GET["error"])){
	if($_GET["error"] == 1){
		echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
        echo 'Necesita iniciar sesion para terminar la compra';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        echo '</div>';
	}else if($_GET["error"] == 2){
		echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
        echo 'Usuario o contraseña incorrectos';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        echo '</div>';
	}else if($_GET["error"] == 3){
	    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
	    echo 'El usuario ha sido deshabilitado ';
	    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	    echo '</div>';
	}
}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
			<div class="col-lg-6 col-md-12">
				<form class="form-signin" action="index.php?pid=<?php echo base64_encode("Presentacion/autenticar.php") ?>" method="post">
            		<h1 class="h3 mb-3 font-weight-normal">INICIO</h1>
            		<!--Campo de correo-->
            		<div class="input-group mb-3">
            			<div class="input-group-prepend email">
            				<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
            			</div>
            			<input type="email" name="correo" id="inputEmail" class="form-control" placeholder="Correo electronico" required autofocus="" title="Ingrese correo" />
            		</div>

            		<!--Campo de contrasea-->
            		<div class="input-group mb-3 contr">
            			<div class="input-group-prepend pass">
            				<span class="input-group-text"><i class="fas fa-key"></i></span>
            			</div>
            			<input type="password" name="clave" id="inputEmail" class="form-control" placeholder="Contrasea" required autofocus="" title="Ingrese Contrasea" />
            		</div>
					<button class="btn btn-lg btn-dark btn-block btn-ingreso" type="submit">
            			Ingresar
            		</button>
    		</form>    
		</div>
	</div>
</div>

