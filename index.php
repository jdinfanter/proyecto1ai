<?php
session_start();
require_once "Logica/Administrador.php";
require_once "Logica/Cliente.php";
require_once "Logica/Encargado.php";
require_once "Logica/Prenda.php";
require_once "Logica/Tipo.php";
require_once "Logica/Talla.php";
require_once "Logica/PrendaTalla.php";
require_once "Logica/Genero.php";
require_once "Logica/Venta.php";
require_once "Logica/VentaPrenda.php";
require_once "Logica/Log.php";
date_default_timezone_set('America/Bogota');
$pid = "";
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
} else {
	$_SESSION["id"] = "";
	$_SESSION["rol"] = "";
}
if (isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])) {
	$_SESSION["id"] = "";
}
?>
<html>

<head>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.	1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
	})
	</script>
</head>

<body>
	<?php
	$paginasSinSesion = array(
		"Presentacion/autenticar.php",
	);
	$paginasCliente = array(
		"Presentacion/inicio.php",
	    "Presentacion/Cliente/InsertarCliente.php",
	);

	if (in_array($pid, $paginasSinSesion)) {
		include $pid;
	} else if ($_SESSION["id"] != "" && !in_array($pid, $paginasCliente)) {
		if ($_SESSION["rol"] == "Administrador") {
			include "Presentacion/menuAdministrador.php";
		} else if ($_SESSION["rol"] == "Cliente") {
		    include "Presentacion/menuCliente.php";
		} else if ($_SESSION["rol"] == "Encargado") {
			include "Presentacion/menuEncargado.php";
		}
		include $pid;
		
	} else {
		$_SESSION["id"] = "";
		include "Presentacion/head.php";
		if (in_array($pid, $paginasCliente)){
			include $pid;
		} else{
			include "Presentacion/tienda.php";
	}
		 
	}
	?>
</body>

</html>