-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-07-2020 a las 18:04:56
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`) VALUES
(1, 'Homero', 'Simpson', '123@123.com', '202cb962ac59075b964b07152d234b70', 'Imagen_Admin/1594767290.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Juan', 'Castro', '111@111.com', '698d51a19d8a121ce581499d7b701668', '', 1),
(2, 'Juanita', 'Jimenez', '112@112.com', '4badaee57fed5610012a296273158f5f', '', 1),
(3, 'Diosdado', 'Cabello', '113@113.com', '73278a4a86960eeb576a8fd4c9ec6997', '', 1),
(4, 'Diomedes', 'Dias', '114@114.com', '5fd0b37cd7dbbb00f97ba6ce92bf5add', '', 1),
(5, 'Falcao', 'Garcia', '115@115.com', '2b44928ae11fb9384c4cf38708677c48', '', 1),
(6, 'Camila', 'Cristancho', '2222@2222.com', '934b535800b1cba8f96a5d72f72f1611', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encargado`
--

CREATE TABLE `encargado` (
  `idEncargado` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `encargado`
--

INSERT INTO `encargado` (`idEncargado`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Carlos', 'Jimenez', '222@222.com', 'bcbe3365e6ac95ea2c0343a2395834dd', '', 1),
(2, 'Gustavo Antonio', 'Bolivar ', '223@223.com', '115f89503138416a242f40fb7d7f338e', '', 1),
(3, 'Pepito', 'Perez', '1234@1234.com', '81dc9bdb52d04dc20036dbd8313ed055', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `idGenero` int(11) NOT NULL,
  `genero` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`idGenero`, `genero`) VALUES
(1, 'hombre'),
(2, 'mujer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `idLog` int(11) NOT NULL,
  `accion` varchar(300) NOT NULL,
  `datos` varchar(300) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `actor` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idLog`, `accion`, `datos`, `fecha`, `hora`, `actor`) VALUES
(1, 'Ingreso al sistema', 'Rol: Administrador', '2020-07-14', '03:52:00', 'Homero Simpson'),
(2, 'Insertar prenda', 'nombre: camisa rosa; precio: 25000; descripcion: Elegante camisa  rosada para caballero; tipo:camisa; talla: s; genero: hombre; foto: ', '2020-07-14', '03:52:00', 'Homero Simpson'),
(3, 'Ingreso al sistema', 'Rol: Cliente', '2020-07-14', '03:53:00', 'Juan Castro'),
(4, 'Registro venta', 'Numero de factura: 2; precio total: 25000', '2020-07-14', '03:53:00', 'Juan Castro'),
(5, 'Ingreso al sistema', 'Rol: Encargado', '2020-07-14', '03:54:00', 'Carlos Jimenez'),
(6, 'Modificar prenda', 'nombre: Camisa Negra; precio: 25000; Descripcion Elegante camisa negra para caballero; tipo:camisa; genero: hombre; foto: ', '2020-07-14', '03:54:00', 'Carlos Jimenez'),
(7, 'Ingreso al sistema', 'Rol: Administrador', '2020-07-14', '03:55:00', 'Homero Simpson'),
(8, 'Ingreso al sistema', 'Rol: Administrador', '2020-07-14', '04:03:00', 'Homero Simpson'),
(9, 'Insertar prenda', 'nombre: pantalon sudadera para mujer; precio: 32000; descripcion: Comodo pantalon de sudadera adidas; tipo:pantalon; talla: s; genero: mujer; foto: ', '2020-07-14', '04:07:00', 'Homero Simpson'),
(10, 'Ingreso al sistema', 'Rol: Administrador', '2020-07-14', '04:09:00', 'Homero Simpson'),
(11, 'Insertar prenda', 'nombre: pantaloneta deportiva para mujer; precio: 28000; descripcion: Practica pantaloneta adidas para hacer ejercicio; tipo:pantaloneta; talla: s; genero: mujer; foto: ', '2020-07-14', '04:11:00', 'Homero Simpson'),
(12, 'Insertar prenda', 'nombre: pantaloneta de baño para hombre; precio: 25000; descripcion: Comoda pantaloneta de baño; tipo:pantaloneta; talla: m; genero: hombre; foto: ', '2020-07-14', '04:12:00', 'Homero Simpson'),
(13, 'Ingreso al sistema', 'Rol: Administrador', '2020-07-14', '05:30:00', 'Homero Simpson'),
(14, 'Insertar prenda', 'nombre: Jean claro para hombre; precio: 80000; descripcion: Jean claro para hombre con raya blanca; tipo:pantalon; talla: S; genero: hombre; foto: ', '2020-07-14', '05:35:00', 'Homero Simpson'),
(15, 'Insertar prenda', 'nombre: Polo azul oscuro; precio: 60000; descripcion: Polo azul oscuro con rayas amarillas ; tipo:camisa; talla: S; genero: hombre; foto: ', '2020-07-14', '05:38:00', 'Homero Simpson'),
(16, 'Insertar prenda', 'nombre: jean claro ; precio: 80000; descripcion: jean claro para mujer; tipo:pantalon; talla: S; genero: mujer; foto: ', '2020-07-14', '05:40:00', 'Homero Simpson'),
(17, 'Insertar prenda', 'nombre: camiseta estampado para mujer; precio: 40000; descripcion: camiseta estampado para mujer color blanca; tipo:camisa; talla: S; genero: mujer; foto: ', '2020-07-14', '05:45:00', 'Homero Simpson'),
(18, 'Ingreso al sistema', 'Rol: Administrador', '2020-07-14', '05:47:00', 'Homero Simpson'),
(19, 'Modificar prenda', 'nombre: camiseta con estampado  ; precio: 40000; Descripcion camiseta estampado para mujer color blanca; tipo:camisa; genero: mujer; foto: ', '2020-07-14', '05:48:00', 'Homero Simpson'),
(20, 'Ingreso al sistema', 'Rol: Cliente', '2020-07-14', '05:49:00', 'Juan Castro'),
(21, 'Registro venta', 'Numero de factura: 3; precio total: 250000', '2020-07-14', '05:49:00', 'Juan Castro'),
(22, 'Ingreso al sistema', 'Rol: Cliente', '2020-07-14', '05:50:00', 'Juanita Jimenez'),
(23, 'Registro venta', 'Numero de factura: 4; precio total: 165000', '2020-07-14', '05:50:00', 'Juanita Jimenez'),
(24, 'Registro venta', 'Numero de factura: 5; precio total: 148000', '2020-07-14', '05:51:00', 'Juanita Jimenez'),
(25, 'Ingreso al sistema', 'Rol: Cliente', '2020-07-14', '05:52:00', 'Falcao Garcia'),
(26, 'Registro venta', 'Numero de factura: 6; precio total: 330000', '2020-07-14', '05:52:00', 'Falcao Garcia'),
(27, 'Ingreso al sistema', 'Rol: Administrador', '2020-07-14', '05:53:00', 'Homero Simpson'),
(28, 'Modificar Administrador', 'Nombre: Homero; Apellido: Simpson; Correo: 123@123.com', '2020-07-14', '05:54:00', 'Homero Simpson'),
(29, 'Ingreso al sistema', 'Rol: Encargado', '2020-07-14', '05:56:00', 'Carlos Jimenez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prenda`
--

CREATE TABLE `prenda` (
  `idPrenda` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `precio` double DEFAULT NULL,
  `descripcion` varchar(300) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `foto` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `idTipo` int(11) NOT NULL,
  `idGenero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `prenda`
--

INSERT INTO `prenda` (`idPrenda`, `nombre`, `precio`, `descripcion`, `foto`, `idTipo`, `idGenero`) VALUES
(1, 'Camisa Negra', 25000, 'Elegante camisa negra para caballero', 'Imagenes_prendas/1594760090.jpg', 1, 1),
(2, 'camisa rosa', 25000, 'Elegante camisa  rosada para caballero', 'Imagenes_prendas/1594759978.jpg', 1, 1),
(3, 'pantalon sudadera para mujer', 32000, 'Comodo pantalon de sudadera adidas', 'Imagenes_prendas/1594760842.jpg', 2, 2),
(4, 'pantaloneta deportiva para mujer', 28000, 'Practica pantaloneta adidas para hacer ejercicio', 'Imagenes_prendas/1594761061.jpg', 3, 2),
(5, 'pantaloneta de baño para hombre', 25000, 'Comoda pantaloneta de baño', 'Imagenes_prendas/1594761169.jpg', 3, 1),
(6, 'Jean claro para hombre', 80000, 'Jean claro para hombre con raya blanca', 'Imagenes_prendas/1594766101.jpg', 2, 1),
(7, 'Polo azul oscuro', 60000, 'Polo azul oscuro con rayas amarillas ', 'Imagenes_prendas/1594766319.jpg', 1, 1),
(8, 'jean claro ', 80000, 'jean claro para mujer', 'Imagenes_prendas/1594766418.jpg', 2, 2),
(9, 'camiseta con estampado  ', 40000, 'camiseta estampado para mujer color blanca', 'Imagenes_prendas/1594766749.jpg', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prenda_talla`
--

CREATE TABLE `prenda_talla` (
  `idPrenda` int(11) NOT NULL,
  `idTalla` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `prenda_talla`
--

INSERT INTO `prenda_talla` (`idPrenda`, `idTalla`, `stock`) VALUES
(1, 1, 149),
(1, 2, 299),
(2, 1, 199),
(2, 2, 199),
(2, 3, 49),
(3, 1, 230),
(4, 1, 119),
(5, 2, 209),
(6, 1, 99),
(6, 2, 49),
(6, 3, 48),
(7, 1, 30),
(7, 2, 45),
(8, 1, 20),
(8, 2, 9),
(9, 1, 19),
(9, 3, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talla`
--

CREATE TABLE `talla` (
  `idTalla` int(11) NOT NULL,
  `talla` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `talla`
--

INSERT INTO `talla` (`idTalla`, `talla`) VALUES
(1, 'S'),
(2, 'M'),
(3, 'L');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idTipo` int(11) NOT NULL,
  `tipo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idTipo`, `tipo`) VALUES
(1, 'camisa'),
(2, 'pantalon'),
(3, 'pantaloneta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idVenta` int(11) NOT NULL,
  `fecha` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `precio_T` double DEFAULT NULL,
  `idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idVenta`, `fecha`, `precio_T`, `idCliente`) VALUES
(1, '2020-07-14', 50000, 1),
(2, '2020-07-14', 25000, 1),
(3, '2020-07-14', 250000, 1),
(4, '2020-07-14', 165000, 2),
(5, '2020-07-14', 148000, 2),
(6, '2020-07-14', 330000, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_prenda`
--

CREATE TABLE `venta_prenda` (
  `idVenta` int(11) NOT NULL,
  `idPrenda` int(11) NOT NULL,
  `idTalla` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta_prenda`
--

INSERT INTO `venta_prenda` (`idVenta`, `idPrenda`, `idTalla`, `cantidad`, `precio`) VALUES
(1, 1, 2, 2, 25000),
(2, 2, 1, 1, 25000),
(3, 1, 2, 1, 25000),
(3, 5, 2, 1, 25000),
(3, 6, 2, 1, 80000),
(3, 7, 2, 2, 60000),
(4, 2, 3, 1, 25000),
(4, 6, 1, 1, 80000),
(4, 7, 2, 1, 60000),
(5, 4, 1, 1, 28000),
(5, 8, 2, 1, 80000),
(5, 9, 1, 1, 40000),
(6, 1, 1, 1, 25000),
(6, 2, 2, 1, 25000),
(6, 6, 3, 2, 80000),
(6, 7, 2, 2, 60000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `encargado`
--
ALTER TABLE `encargado`
  ADD PRIMARY KEY (`idEncargado`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idGenero`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`idLog`);

--
-- Indices de la tabla `prenda`
--
ALTER TABLE `prenda`
  ADD PRIMARY KEY (`idPrenda`),
  ADD KEY `idTipo` (`idTipo`),
  ADD KEY `idGenero` (`idGenero`);

--
-- Indices de la tabla `prenda_talla`
--
ALTER TABLE `prenda_talla`
  ADD PRIMARY KEY (`idPrenda`,`idTalla`),
  ADD KEY `idTalla` (`idTalla`);

--
-- Indices de la tabla `talla`
--
ALTER TABLE `talla`
  ADD PRIMARY KEY (`idTalla`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idVenta`),
  ADD KEY `idCliente` (`idCliente`);

--
-- Indices de la tabla `venta_prenda`
--
ALTER TABLE `venta_prenda`
  ADD PRIMARY KEY (`idVenta`,`idPrenda`),
  ADD KEY `idPrenda` (`idPrenda`),
  ADD KEY `idTalla` (`idTalla`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `encargado`
--
ALTER TABLE `encargado`
  MODIFY `idEncargado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `idGenero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `idLog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `prenda`
--
ALTER TABLE `prenda`
  MODIFY `idPrenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `talla`
--
ALTER TABLE `talla`
  MODIFY `idTalla` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idVenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `prenda`
--
ALTER TABLE `prenda`
  ADD CONSTRAINT `prenda_ibfk_1` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idTipo`),
  ADD CONSTRAINT `prenda_ibfk_2` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`);

--
-- Filtros para la tabla `prenda_talla`
--
ALTER TABLE `prenda_talla`
  ADD CONSTRAINT `prenda_talla_ibfk_1` FOREIGN KEY (`idPrenda`) REFERENCES `prenda` (`idPrenda`),
  ADD CONSTRAINT `prenda_talla_ibfk_2` FOREIGN KEY (`idTalla`) REFERENCES `talla` (`idTalla`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
