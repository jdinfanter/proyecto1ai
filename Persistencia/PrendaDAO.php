<?php 
class PrendaDAO{
    private $idPrenda;
    private $nombre;
    private $precio;
    private $descripcion;
    private $foto;
    private $tipo;
    private $genero;

    public function PrendaDAO($idPrenda = "", $nombre = "", $precio = "", $descripcion = "", $foto ="", $tipo = "", $genero = ""){
        $this -> idPrenda = $idPrenda;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> descripcion = $descripcion;
        $this -> foto = $foto;
        $this -> tipo = $tipo;
        $this -> genero = $genero;
        
    }

    public function consultar(){
        return "select p.idPrenda, p.nombre, p.precio, p.descripcion, p.foto, p.idTipo, t.tipo, g.genero, p.idGenero
            from Prenda p, Tipo t, Genero g
            where t.idTipo = p.idTipo AND g.idGenero = p.idGenero AND idPrenda = '" . $this -> idPrenda .  "'";
    }    
    
    public function insertar(){
        return "insert into Prenda (nombre, precio, descripcion, foto, idTipo, idGenero)
                values ('" . $this -> nombre . "', '" . $this -> precio . "', '" . $this -> descripcion . "', '" . $this -> foto . "',  '" . $this -> tipo . "','" . $this -> genero . "')";
    }
    
    public function consultarTodos(){
        return "select idPrenda, nombre, precio, descripcion, foto, idTipo, idGenero
                from Prenda";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select p.idPrenda, p.nombre, p.precio, p.descripcion, p.foto, p.idTipo, t.tipo, g.genero, p.idGenero
                from Prenda p, Tipo t, Genero g
                where t.idTipo = p.idTipo AND  g.idGenero = p.idGenero
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarFiltro($cantidad, $pagina, $filtro, $filtro2){
        return "select p.idPrenda, p.nombre, p.precio, p.descripcion, p.foto, p.idTipo, t.tipo, g.genero, p.idGenero
        from Prenda p, Tipo t, Genero g
        where t.idTipo = p.idTipo AND p.idGenero = g.idGenero AND t.tipo like '%".$filtro."' AND g.genero like '%".$filtro2."'  
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        // return "select p.idPrenda, p.nombre, p.precio, p.descripcion, p.foto, p.idTipo, t.tipo
        //         from Prenda p, Tipo t
        //         where t.idTipo = p.idTipo ".($filtro == "todo")?"":"p.tipo like '%".$filtro."'"."
        //         limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idPrenda)
                from Prenda";
    }

    public function consultarId(){
        return "select @@identity AS id";
    }
 
    public function editar(){
        return "update Prenda
                set nombre = '" . $this -> nombre . "', precio = '" . $this -> precio . "', descripcion = '" . $this -> descripcion . "', foto = '" . $this -> foto . "', idTipo = '" . $this -> tipo . "', idGenero = '" . $this -> genero ."'
                where idPrenda = '" . $this -> idPrenda .  "'";
    }


}
