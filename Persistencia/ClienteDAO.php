<?php
class ClienteDAO{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function ClienteDAO($idCliente = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idCliente = $idCliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from Cliente
                where correo = '" . $this -> correo .  "'";
    }
        
    public function registrar(){
        return "insert into Cliente (nombre, apellido, correo, clave,foto, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> foto . "', '1')";
    }
 
 
    public function autenticar(){
        return "select idCliente, estado, nombre, apellido
                from Cliente
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, clave,foto, estado
                from Cliente
                where idcliente = '" . $this -> idCliente .  "'";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idCliente, nombre, apellido, correo, estado
                from Cliente
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idCliente)
                from Cliente";
    }
    
    public function editar(){
        return "update Cliente
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "',  foto = '" . $this -> foto . "', estado = '" . $this -> estado . "' 
                where idCliente = '" . $this -> idCliente .  "'";
    }
    
    public function editar_clave(){
        return "update Cliente
                set clave = '" . md5($this -> clave) . "'
                where idCliente = '" . $this -> idCliente .  "'";
    }

    public function cambiarEstado($estado){
        return "update Cliente
                set estado = '" . $estado . "'
                where idCliente = '" . $this -> idCliente .  "'";
    }
    
}

?>