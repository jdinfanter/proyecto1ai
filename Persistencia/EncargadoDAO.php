<?php
class EncargadoDAO{
    private $idEncargado;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function EncargadoDAO($idEncargado= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idEncargado = $idEncargado;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from Encargado
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar(){
        return "insert into Encargado (nombre, apellido, correo, clave,foto, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> foto . "', '1')";
    }
    
    
    public function autenticar(){
        return "select idEncargado, estado, nombre, apellido
                from Encargado
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, clave, foto, estado
                from Encargado
                where idEncargado = '" . $this -> idEncargado .  "'";
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idEncargado, nombre, apellido, correo, estado
                from Encargado
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idEncargado)
                from Encargado";
    }
    
    public function editar(){
        return "update Encargado
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', foto= '" . $this -> foto . "', estado = '" . $this -> estado . "'
                where idEncargado = '" . $this -> idEncargado .  "'";
    }
    
    public function editar_clave(){
        return "update Encargado
                set clave = '" . md5($this -> clave) . "'
                where idEncargado = '" . $this -> idEncargado .  "'";
    }

    public function cambiarEstado($estado){
        return "update Encargado
                set estado = '" . $estado . "'
                where idEncargado = '" . $this -> idEncargado .  "'";
    }
    
}

?>