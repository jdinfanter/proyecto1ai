<?php
class VentaDAO{
    private $idVenta;
    private $fecha;
    private $precioT;
    private $cliente;

    public function VentaDAO($idVenta="", $fecha="", $precioT="", $cliente){
        $this -> idVenta = $idVenta;
        $this -> fecha = $fecha;
        $this -> precioT = $precioT;
        $this -> cliente = $cliente;
    }

    public function insertar(){
        return "insert into Venta (fecha, precio_T, idCliente)
                values ('" . $this -> fecha . "', '" . $this -> precioT . "', '" . $this -> cliente . "')";
    }

    public function consultarId(){
        return "select @@identity AS id";
    }

    public function consultarPaginacion($cantidad, $pagina){
        return "select v.idVenta, v.fecha, v.precio_T, c.nombre, c.apellido
                from venta v, cliente c
                where v.idCliente = c.idCliente
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idVenta)
                from Venta";
    }
    
   

}
?>